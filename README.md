# GitHub 2 GitLab

Userstyle to turn GitLab into GitHub. Or make gitlab.com look like github.com. Same thing.

## Contents
* [Installation Guide](#installation-guide)
*    [Prerequisites](#prerequisites)
*    [Installing](#installing)
* [Built With](#built-with)
* [Contributing](#contributing)
* [Versioning](#versioning)
* [Authors](#authors)
* [License](#license)
* [Project History](#project-history)

## Installation Guide

### Prerequisites

This works through the browser extension [Stylus](https://add0n.com/stylus.html), which is available for:
* [Mozilla Firefox](https://addons.mozilla.org/addon/styl-us/)
* [Opera](https://addons.opera.com/extensions/details/stylus/)

### Installing

Once you have installed the Stylus addon, you just have to open the userstyle in the browser.

Opening the userstyle in the browser triggers the Stylus addon which then asks you to install the userstyle.

[Click here to open the userstyle](https://gitlab.com/maxvalue-copyright/github2gitlab/-/raw/master/github2gitlab.user.styl).

## Built With

* [Mozilla Firefox](https://firefox.com/)
* [Stylus](https://add0n.com/stylus.html)

## Contributing

Please open an issue if you want to help.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/maxvalue-copyright/github2gitlab/-/tags).

## Authors

* **Max Fuxjäger** - *Initial work* - [MaxValue](https://gitlab.com/MaxValue/)

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.

## Project History
This project was created because I (Max) thought that the GitHub design is much less cognitive stressing than the
GitLab design. Honestly, the GitLab design is much to crowded and small.
